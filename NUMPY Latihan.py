#!/usr/bin/env python
# coding: utf-8

# In[1]:


pip install numpy


# In[12]:


import numpy as np
my_list = [1,2,3]
my_list


# In[14]:


np.array(my_list)


# In[16]:


my_matrix = [[1,2,3],[4,5,6],[7,8,9]]
my_matrix


# In[18]:


np.array(my_matrix)


# In[21]:


np.arange(0,10)


# In[23]:


np.arange(0,11,2)


# In[25]:


np.zeros(3)


# In[27]:


np.zeros((5,5))


# In[29]:


np.ones(3)


# In[31]:


np.ones((3,3))


# In[33]:


np.linspace(0,10,3)


# In[35]:


np.linspace(0,10,50)


# In[37]:


np.eye(4)


# In[39]:


np.random.rand(2)


# In[41]:


np.random.rand(5,5)


# In[43]:


np.random.randn(2)


# In[45]:


np.random.randn(5,5)


# In[47]:


np.random.randint(1,100)


# In[49]:


np.random.randint(1,100,10)


# In[54]:


arr = np.arange(25)
ranarr = np.random.randint(0,50,10)
arr


# In[56]:


ranarr


# In[59]:


arr.reshape(5,5)


# In[61]:


ranarr


# In[63]:


ranarr.max()


# In[65]:


ranarr.argmax()


# In[67]:


ranarr.min()


# In[69]:


ranarr.argmin()


# In[71]:


arr.shape


# In[73]:


arr.reshape(1,25)


# In[75]:


arr.reshape(1,25).shape


# In[77]:


arr.reshape(25,1)


# In[79]:


arr.reshape(25,1).shape


# In[81]:


arr.dtype


# In[88]:


arr = np.arange(0,11)
arr


# In[90]:


arr[8]


# In[92]:


arr[1:5]


# In[94]:


arr[0:5]


# In[97]:


arr[0:5]=100
arr


# In[99]:


arr = np.arange(0,11)
arr


# In[101]:


slice_of_arr = arr[0:6]
slice_of_arr


# In[103]:


slice_of_arr[:]=99
slice_of_arr


# In[105]:


arr


# In[107]:


arr_copy = arr.copy()
arr_copy


# In[109]:


arr_2d = np.array(([5,10,15],[20,25,30],[35,40,45]))
arr_2d


# In[110]:


arr_2d[1]


# In[112]:


arr_2d[1][0]


# In[113]:


arr_2d[1,0]


# In[115]:


arr_2d[:2,1:]


# In[116]:


arr_2d[2]


# In[117]:


arr_2d[2,:]


# In[118]:


arr = np.arange(1,11)
arr


# In[119]:


arr > 4


# In[120]:


bool_arr = arr>4
bool_arr


# In[121]:


arr[bool_arr]


# In[122]:


arr[arr>2]


# In[123]:


x= 2
arr[arr>x]


# In[124]:


arr = np.arange(0,10)
arr + arr


# In[125]:


arr * arr


# In[126]:


arr - arr


# In[127]:


arr / arr


# In[129]:


1/arr


# In[131]:


arr ** 3


# In[132]:


np.sqrt(arr)


# In[133]:


np.exp(arr)


# In[134]:


np.max(arr)


# In[135]:


np.sin(arr)


# In[136]:


np.log(arr)

