#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np


# In[3]:


np.zeros(10)


# In[5]:


np.ones(10)


# In[11]:


arr = np.arange(0,10)
fives = arr[0:10]
fives[:]=5
fives


# In[14]:


np.arange(10,50)


# In[15]:


np.arange(10,50,2)


# In[17]:


my_matrix = [[0,1,2],[3,4,5],[6,7,8]]
np.array(my_matrix)


# In[19]:


np.eye(3)


# In[48]:


np.random.randn(1)


# In[49]:


np.random.randn(5,5)


# In[56]:


arr = np.linspace(0.01, 1.00, 100)
arr.reshape(10,10)


# In[58]:


arr = np.linspace(0.0, 1.0, 20)
arr.reshape(5,4)


# In[59]:


mat = np.arange(1,26).reshape(5,5)
mat


# In[70]:


mat = np.arange(1,26).reshape(5,5)
mat[2:,1:]


# In[73]:


mat[3][4]


# In[77]:


mat[:3,1:2]


# In[78]:


mat[4]


# In[79]:


mat[3:]


# In[87]:


mat = np.arange(1,26).reshape(5,5)
mat


# In[88]:


sum(sum(mat))


# In[89]:


mat.std()


# In[90]:


sum(mat)

